# -*- coding: utf-8 -*-
# !/usr/bin/python
import hashlib
import uuid

from classes.block import Block
from classes.wallet import Wallet
from settings.settings import Settings


class Chain:

    def __init__(self, blocks):
        self.blocks = [blocks]
        self.last_transaction_number = ''

    @staticmethod
    def get_hash():
        """
        Génère un hash en fonction d'un string aléatoire
        :return: string
        """
        h = hashlib.sha256(Settings.create_base_hash().encode())
        h = h.hexdigest()
        # print(h)
        return h

    def verify_hash(self):
        """
        Vérifie que le hash corresponde bien.
        :return: Boolean
        """
        if self.get_hash() == self.hash:
            return True
        else:
            return False

    def add_block(self):
        pass

    @staticmethod
    def get_block(hash):
        block = Block(hash)
        datas = {
            "hash": block.hash,
            "base_hash": block.base_hash,
            "parent_hash": block.parent_hash,
            "transactions": block.transactions_dict
        }
        print(datas)
        return datas


# TODO : Adapter cette méthode aux blocks (hash_emitor, hsh_receptor, etc..)
def add_transaction(self, emetteur, recepteur, montant):
    """
        :param emetteur: string
        :param recepteur: string
        :param montant: integer
        :param id: integer
        :return:
        """
    wallet_emitter = Wallet(emetteur)
    wallet_receptor = Wallet(recepteur)
    t = uuid.uuid1()
    self.transactions = str(t)
    datas = {
        self.transactions: {
            "wallet_emitter": emetteur,
            "wallet_receptor": recepteur,
            "montant": montant,
            "ID de la transaction": self.id_transaction
        },
    }

    test = wallet_emitter.send(datas, montant, 'emitter')

    # print(test)
    if not test:
        print('\n\t Echec de la transaction')

    else:

        wallet_receptor.send(datas, montant, 'receptor')
        self.transactions_dict.update(datas)
        print('\n\t Transaction réussie.')
        self.save()
