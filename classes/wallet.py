# -*- coding: utf-8 -*-
# !/usr/bin/python

import json
import os
import uuid
from pprint import pprint

from settings.settings import Settings


class Wallet:

    def __init__(self, id="", balance=100, history=[]):

        if id == "":
            self.unique_id = self.generate_unique_id()
            self.balance = balance
            self.history = history
            self.save()
        else:
            self.load(id)
            self.unique_id = id

    def generate_unique_id(self):
        unique_id = uuid.uuid1()
        unique_id = str(unique_id)
        file_exist = os.path.exists(f'content/wallets/{unique_id}.json')
        if file_exist:
            self.generate_unique_id()
        else:
            # print(unique_id)
            return unique_id

    def add_balance(self, amount):
        """
        Méthode qui vérifie si l'entrée est bien de type integer, puis rejette
        ou valide la transaction en fonction (ajoute le montant indiqué).
         Affiche ensuite la balance du Wallet.
        :param amount:
        """
        if isinstance(amount, int):
            self.balance += amount
            print('\n\t Transaction validée.')
        else:
            print('\n\t Echec, Information non-valide...')
        print(self.balance)

    def sub_balance(self, sub_amount):
        """
        Méthode qui vérifie si l'entrée est bien de type integer,
         puis vérifie si le solde disponible
        permet la transaction demandée, rejette ensuite
        ou valide la transaction en fonction (soustrait le montant indiqué).
        Affiche ensuite la balance du Wallet.
        :param : sub_amount
        """
        if not isinstance(sub_amount, int):
            return False
        else:
            if self.balance - sub_amount < 0:
                print('\n\t Echec, le solde disponible '
                      'n\'est pas suffisant...')
                return False
            else:
                self.balance -= sub_amount
                print('\n\t Transaction validée.')
                return True

    """def send(self, valeur):
        self.history.append(valeur)"""

    def send(self, transaction, montant, role):

        if role == 'emitter':
            if not self.sub_balance(montant):
                return False
        elif role == 'receptor':
            self.add_balance(montant)
        else:
            print("Erreur")

        self.history.append(transaction)
        self.save()

    def save(self):
        """
        Sauvegarde la transaction dans un fichier json
        :return:
        """
        data = {
            'unique_id': self.unique_id,
            'balance': self.balance,
            'history': self.history
        }

        file_path = f'{Settings.get_wallets_path()}{self.unique_id}.json'

        with open(file_path, 'w+') as outfile:
            to_str = json.dumps(data,
                                indent=4, sort_keys=True,
                                separators=(',', ': '), ensure_ascii=False)
            outfile.write(to_str)
            # print(to_str)

    def load(self, id_file):
        """
        Permet de récupérer les informations
        :param: id_file: string
        :return: datas_target_wallet : dictionary
        """
        with open(f"{Settings.get_wallets_path()}{id_file}.json") as file:
            datas_target_wallet = json.load(file)
        self.balance = datas_target_wallet['balance']
        self.history = datas_target_wallet['history']
        pprint(datas_target_wallet)
        return datas_target_wallet
