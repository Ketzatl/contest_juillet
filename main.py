# -*- coding: utf-8 -*-
# !/usr/bin/python

from classes.block import Block

from settings.settings import Settings


# b1 = Block('', 'pouet', 'piouf', '')
# b1.load(block_test_1)

# b1.add_transaction(w_test1, w_test2, 10)
# b1.get_weight()


# w1 = Wallet()


# w1.load('24e0d9a0-dfd6-11eb-9f02-a0999b1cc837')
# print(w1.balance)
# print(w1.history)


# b1 = Block("", "pouet", "pouet", "")
# b1.load('2338ed83dc3159af6d62e08182b36cbf8b6457f6da8069b2289dea989af31e81')

# b1 = Block()
# b1.create_base_hash()


def main():
    b1 = Block('', 'pouet', 'piouf', '', '')
    # b1.load(Settings.get_block_hash_test_1())
    b1.add_transaction(Settings.get_wallet_id_test_2(),
                       Settings.get_wallet_id_test_1(), 1)
    # b1.get_transaction(1)


main()
